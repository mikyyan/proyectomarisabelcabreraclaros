<?php

namespace App\Http\Controllers;

use App\auto;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
class AutoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function index()
    {
        //
       
        $dato['auto']=auto::paginate(5);

        return view('auto.index',$dato);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('auto.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        $campos=[
            'placa' => 'required|string|max:100',
            'modelo' => 'required|string|max:100',
            'marca' => 'required|string|max:100',
            'cilindraje' => 'required|string|max:100',
            'estado' => 'required|string|max:100',
            'color' => 'required|string|max:100',
            'foto' => 'required|max:10000|mimes:jpeg,png,jpg',
        ];
        $Mensaje=["required"=>'The :attribute is required'];
        $this->validate($request,$campos,$Mensaje);
        //$datosauto=request()->all();

        $datosauto=request()->except('_token');

        if($request->hasFile('foto')){
            $datosauto['foto']=$request->file('foto')->store('uploads','public');
        }
        
        auto::insert($datosauto);

        //return response()->json($datosauto);
        return redirect('auto')->with('Mensaje','Auto agregado con exito');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\auto  $auto
     * @return \Illuminate\Http\Response
     */
    public function show(auto $auto)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\auto  $auto
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $auto= auto::findOrfail($id);

        return view('auto.edit',compact('auto'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\auto  $auto
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $campos=[
            'placa' => 'required|string|max:100',
            'modelo' => 'required|string|max:100',
            'marca' => 'required|string|max:100',
            'cilindraje' => 'required|string|max:100',
            'estado' => 'required|string|max:100',
            'color' => 'required|string|max:100',
        ];
        
        if($request->hasFile('foto')){
            $campos+=['foto' => 'required|max:10000|mimes:jpeg,png,jpg'];
        }
        $Mensaje=["required"=>'The :attribute is required'];
        $this->validate($request,$campos,$Mensaje);

        
        $datosauto=request()->except(['_token','_method']);

        if($request->hasFile('foto')){
            $auto= auto::findOrfail($id);

            Storage::delete('public/'.$auto->foto);

            $datosauto['foto']=$request->file('foto')->store('uploads','public');
        }

        auto::where('id','=',$id)->update($datosauto);
        
        //$auto= auto::findOrfail($id);
        //return view('auto.edit',compact('auto'));
        return redirect('auto')->with('Mensaje','Auto modificado con exito');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\auto  $auto
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $auto= auto::findOrfail($id);
        if(Storage::delete('public/'.$auto->foto)){
            auto::destroy($id);
        }

        return redirect('auto')->with('Mensaje','Auto eliminado con exito');
    }
}
