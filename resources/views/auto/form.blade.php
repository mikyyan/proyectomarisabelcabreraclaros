
<div class="form-group">
<label for="placa" class="control-label">{{'Placa'}}</label>
<input type="text" class="form-control {{ $errors->has('placa')?'is-invalid':'' }}" 
name="placa" 
id="placa" 
value="{{ isset($auto->placa)?$auto->placa:old('placa') }}"
>
{!! $errors->first('placa','<div class="invalid-feedback">:message</div>') !!}
</div>

<div class="form-group">
<label for="modelo" class="control-label">{{'Modelo'}}</label>
<input type="text" class="form-control {{ $errors->has('modelo')?'is-invalid':'' }}" 
name="modelo" id="modelo" 
value="{{ isset($auto->modelo)?$auto->modelo:old('modelo') }}"
>
{!! $errors->first('modelo','<div class="invalid-feedback">:message</div>') !!}
</div>

<div class="form-group">
<label for="marca" class="control-label">{{'Marca'}}</label>
<input type="text" class="form-control {{ $errors->has('marca')?'is-invalid':'' }}" 
name="marca" id="marca" 
value="{{ isset($auto->marca)?$auto->marca:old('marca') }}"
>
{!! $errors->first('marca','<div class="invalid-feedback">:message</div>') !!}
</div>

<div class="form-group">
<label for="cilindraje" class="control-label">{{'Cilindraje'}}</label>
<input type="text" class="form-control {{ $errors->has('cilindraje')?'is-invalid':'' }}" 
name="cilindraje" id="cilindraje" 
value="{{ isset($auto->cilindraje)?$auto->cilindraje:old('cilindraje') }}"
>
{!! $errors->first('cilindraje','<div class="invalid-feedback">:message</div>') !!}
</div>

<div class="form-group">
<label for="estado" class="control-label">{{'Estado'}}</label>
<input type="text" class="form-control {{ $errors->has('estado')?'is-invalid':'' }}" 
name="estado" id="estado" 
value="{{ isset($auto->estado)?$auto->estado:old('estado') }}"
>
{!! $errors->first('estado','<div class="invalid-feedback">:message</div>') !!}
</div>

<div class="form-group">
<label for="color" class="control-label">{{'Color'}}</label>
<input type="text" class="form-control {{ $errors->has('color')?'is-invalid':'' }}" 
name="color" id="color" 
value="{{ isset($auto->color)?$auto->color:old('color') }}"
>
{!! $errors->first('color','<div class="invalid-feedback">:message</div>') !!}
</div>


<div class="form-group">
<label for="foto" class="control-label">{{'Foto'}}</label>

@if(isset($auto->foto))
<br/>
<img class="img-thumbnail img-fluid" src="{{ asset('storage').'/'.$auto->foto}}" alt="" width="200">
<br/>
@endif

<input class="form-control {{ $errors->has('foto')?'is-invalid':'' }}"
type="file" class="form-control" name="foto" id="foto" value="">
{!! $errors->first('foto','<div class="invalid-feedback">:message</div>') !!}
</div>

<input type="submit" class="btn btn-success" value="{{ $Modo=='crear' ? 'Agregar':'Modificar'}}">
<a class="btn btn-primary" href="{{ url('auto') }}">Regresar</a>